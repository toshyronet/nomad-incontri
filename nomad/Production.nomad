job "incontri" {
  
  datacenters = ["dc1"]
  type = "service"
  
  group "incontri" {
    count = 2

    update {
      max_parallel     = 1
      canary           = 1
      min_healthy_time = "30s"
      healthy_deadline = "5m"
      auto_revert      = true
      auto_promote     = true
    }

    service {
      name = "incontri"
      port = "APP"

      tags = ["app"]
      canary_tags = ["canary"]

      check {
        type     = "http"
        port     = "APP"
        path     = "/parameters"
        interval = "5s"
        timeout  = "2s"
      }
    }
	
    network {
      port "APP" {}
    }

    task "incontri" {

      driver = "docker"

      config {
        image = "toshyrodocker/incontri-api:latest"
        ports = ["APP"]
      }

      resources {
        memory = 256
      }
    }
  }
}